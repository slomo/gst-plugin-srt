// Copyright (C) 2022 Sebastian Dröge <sebastian@centricular.com>
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0
#![allow(unused)]

mod ffi;

use std::{borrow::Cow, sync::atomic};

static STARTUP_COUNT: atomic::AtomicU64 = atomic::AtomicU64::new(0);

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[repr(i32)]
pub enum Error {
    UNKNOWN = ffi::EUNKNOWN,
    CONNSETUP = ffi::ECONNSETUP,
    NOSERVER = ffi::ENOSERVER,
    CONNREJ = ffi::ECONNREJ,
    SOCKFAIL = ffi::ESOCKFAIL,
    SECFAIL = ffi::ESECFAIL,
    SCLOSED = ffi::ESCLOSED,
    CONNFAIL = ffi::ECONNFAIL,
    CONNLOST = ffi::ECONNLOST,
    NOCONN = ffi::ENOCONN,
    RESOURCE = ffi::ERESOURCE,
    THREAD = ffi::ETHREAD,
    NOBUF = ffi::ENOBUF,
    SYSOBJ = ffi::ESYSOBJ,
    FILE = ffi::EFILE,
    INVRDOFF = ffi::EINVRDOFF,
    RDPERM = ffi::ERDPERM,
    INVWROFF = ffi::EINVWROFF,
    WRPERM = ffi::EWRPERM,
    INVOP = ffi::EINVOP,
    BOUNDSOCK = ffi::EBOUNDSOCK,
    CONNSOCK = ffi::ECONNSOCK,
    INVPARAM = ffi::EINVPARAM,
    INVSOCK = ffi::EINVSOCK,
    UNBOUNDSOCK = ffi::EUNBOUNDSOCK,
    NOLISTEN = ffi::ENOLISTEN,
    RDVNOSERV = ffi::ERDVNOSERV,
    RDVUNBOUND = ffi::ERDVUNBOUND,
    INVALMSGAPI = ffi::EINVALMSGAPI,
    INVALBUFFERAPI = ffi::EINVALBUFFERAPI,
    DUPLISTEN = ffi::EDUPLISTEN,
    LARGEMSG = ffi::ELARGEMSG,
    INVPOLLID = ffi::EINVPOLLID,
    POLLEMPTY = ffi::EPOLLEMPTY,
    BINDCONFLICT = ffi::EBINDCONFLICT,
    ASYNCFAIL = ffi::EASYNCFAIL,
    ASYNCSND = ffi::EASYNCSND,
    ASYNCRCV = ffi::EASYNCRCV,
    TIMEOUT = ffi::ETIMEOUT,
    CONGEST = ffi::ECONGEST,
    PEERERR = ffi::EPEERERR,
}

impl From<Error> for i32 {
    fn from(v: Error) -> Self {
        v as i32
    }
}

impl From<i32> for Error {
    fn from(v: i32) -> Error {
        assert_ne!(v, 0);

        match v {
            ffi::ECONNSETUP => Error::CONNSETUP,
            ffi::ENOSERVER => Error::NOSERVER,
            ffi::ECONNREJ => Error::CONNREJ,
            ffi::ESOCKFAIL => Error::SOCKFAIL,
            ffi::ESECFAIL => Error::SECFAIL,
            ffi::ESCLOSED => Error::SCLOSED,
            ffi::ECONNFAIL => Error::CONNFAIL,
            ffi::ECONNLOST => Error::CONNLOST,
            ffi::ENOCONN => Error::NOCONN,
            ffi::ERESOURCE => Error::RESOURCE,
            ffi::ETHREAD => Error::THREAD,
            ffi::ENOBUF => Error::NOBUF,
            ffi::ESYSOBJ => Error::SYSOBJ,
            ffi::EFILE => Error::FILE,
            ffi::EINVRDOFF => Error::INVRDOFF,
            ffi::ERDPERM => Error::RDPERM,
            ffi::EINVWROFF => Error::INVWROFF,
            ffi::EWRPERM => Error::WRPERM,
            ffi::EINVOP => Error::INVOP,
            ffi::EBOUNDSOCK => Error::BOUNDSOCK,
            ffi::ECONNSOCK => Error::CONNSOCK,
            ffi::EINVPARAM => Error::INVPARAM,
            ffi::EINVSOCK => Error::INVSOCK,
            ffi::EUNBOUNDSOCK => Error::UNBOUNDSOCK,
            ffi::ENOLISTEN => Error::NOLISTEN,
            ffi::ERDVNOSERV => Error::RDVNOSERV,
            ffi::ERDVUNBOUND => Error::RDVUNBOUND,
            ffi::EINVALMSGAPI => Error::INVALMSGAPI,
            ffi::EINVALBUFFERAPI => Error::INVALBUFFERAPI,
            ffi::EDUPLISTEN => Error::DUPLISTEN,
            ffi::ELARGEMSG => Error::LARGEMSG,
            ffi::EINVPOLLID => Error::INVPOLLID,
            ffi::EPOLLEMPTY => Error::POLLEMPTY,
            ffi::EBINDCONFLICT => Error::BINDCONFLICT,
            ffi::EASYNCFAIL => Error::ASYNCFAIL,
            ffi::EASYNCSND => Error::ASYNCSND,
            ffi::EASYNCRCV => Error::ASYNCRCV,
            ffi::ETIMEOUT => Error::TIMEOUT,
            ffi::ECONGEST => Error::CONGEST,
            ffi::EPEERERR => Error::PEERERR,
            _ => Error::UNKNOWN,
        }
    }
}

impl std::error::Error for Error {}
impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        unsafe {
            use std::ffi::CStr;

            if *self == Error::UNKNOWN {
                return f.write_str("UNKNOWN ERROR");
            }

            let s = ffi::srt_strerror(i32::from(*self), -1);
            let s = CStr::from_ptr(s).to_str().unwrap();
            f.write_str(s)
        }
    }
}

impl Error {
    pub fn last_error() -> Result<(), Error> {
        unsafe {
            let mut errno = 0;
            let code = ffi::srt_getlasterror(&mut errno);
            if code == 0 {
                Ok(())
            } else {
                ffi::srt_clearlasterror();
                Err(Error::from(code))
            }
        }
    }
}

pub fn init() {
    static ONCE: std::sync::Once = std::sync::Once::new();

    ONCE.call_once(|| {
        static CAT: once_cell::sync::Lazy<gst::DebugCategory> = once_cell::sync::Lazy::new(|| {
            gst::DebugCategory::new("srtlib", gst::DebugColorFlags::empty(), Some("SRT library"))
        });

        unsafe extern "C" fn handler(
            opaque: *mut libc::c_void,
            level: i32,
            file: *const libc::c_char,
            line: i32,
            area: *const libc::c_char,
            msg: *const libc::c_char,
        ) {
            let level = match level {
                2 => gst::DebugLevel::Error,
                3 => gst::DebugLevel::Warning,
                4 => gst::DebugLevel::Info,
                5 => gst::DebugLevel::Debug,
                7 => gst::DebugLevel::Log,
                _ => gst::DebugLevel::Fixme,
            };

            let file = glib::GStr::from_ptr(file as *const _);
            let area = glib::GStr::from_ptr(area as *const _);
            let msg = glib::GStr::from_ptr(msg as *const _);

            CAT.log(
                None::<&gst::Object>,
                level,
                file,
                area,
                line as u32,
                format_args!("{}", msg),
            );
        }

        unsafe {
            ffi::srt_setloghandler(std::ptr::null_mut(), Some(handler));
            ffi::srt_setloglevel(7);
            ffi::srt_setlogflags(0x1 | 0x2 | 0x4 | 0x8);
        }
    });
}

pub fn startup() -> Result<(), Error> {
    unsafe {
        if STARTUP_COUNT.fetch_add(1, atomic::Ordering::SeqCst) == 0 {
            if ffi::srt_startup() < 0 {
                STARTUP_COUNT.store(0, atomic::Ordering::SeqCst);
                Err(Error::UNKNOWN)
            } else {
                Ok(())
            }
        } else {
            Ok(())
        }
    }
}

pub fn shutdown() -> Result<(), Error> {
    unsafe {
        if STARTUP_COUNT.fetch_sub(1, atomic::Ordering::SeqCst) == 1 {
            if ffi::srt_cleanup() < 0 {
                STARTUP_COUNT.store(0, atomic::Ordering::SeqCst);
                Err(Error::UNKNOWN)
            } else {
                Ok(())
            }
        } else {
            Ok(())
        }
    }
}

pub fn version() -> u32 {
    unsafe { ffi::srt_getversion() }
}

pub fn time_now() -> i64 {
    unsafe { ffi::srt_time_now() }
}

#[repr(transparent)]
#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Socket(ffi::Socket);

unsafe impl Send for Socket {}

impl Drop for Socket {
    fn drop(&mut self) {
        unsafe {
            ffi::srt_close(self.0);
        }
    }
}

impl Socket {
    pub fn new() -> Result<Socket, Error> {
        unsafe {
            let res = ffi::srt_create_socket();
            if res != -1 {
                Ok(Socket(res))
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    pub fn bind(&mut self, addr: std::net::SocketAddr) -> Result<(), Error> {
        unsafe {
            use std::mem;

            let res = match addr {
                std::net::SocketAddr::V4(addr) => {
                    let addr = libc::sockaddr_in {
                        sin_family: libc::AF_INET as u16,
                        sin_port: u16::to_be(addr.port()),
                        sin_addr: libc::in_addr {
                            s_addr: u32::from_ne_bytes(addr.ip().octets()),
                        },
                        sin_zero: [0; 8],
                    };

                    ffi::srt_bind(
                        self.0,
                        &addr as *const _ as *const _,
                        mem::size_of_val(&addr) as i32,
                    )
                }
                std::net::SocketAddr::V6(addr) => {
                    let addr = libc::sockaddr_in6 {
                        sin6_family: libc::AF_INET6 as u16,
                        sin6_port: u16::to_be(addr.port()),
                        sin6_flowinfo: 0,
                        sin6_addr: libc::in6_addr {
                            s6_addr: addr.ip().octets(),
                        },
                        sin6_scope_id: 0,
                    };

                    ffi::srt_bind(
                        self.0,
                        &addr as *const _ as *const _,
                        mem::size_of_val(&addr) as i32,
                    )
                }
            };

            if res == 0 {
                Ok(())
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    pub fn listen(&mut self, backlog: i32) -> Result<(), Error> {
        unsafe {
            let res = ffi::srt_listen(self.0, backlog);

            if res == 0 {
                Ok(())
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    pub fn accept(&mut self) -> Result<(Socket, std::net::IpAddr, u16), Error> {
        unsafe {
            use std::mem;

            let mut addr: mem::MaybeUninit<libc::sockaddr_storage> = mem::MaybeUninit::zeroed();
            let mut len = mem::size_of::<libc::sockaddr_storage>() as i32;
            let res = ffi::srt_accept(self.0, addr.as_mut_ptr() as *mut _, &mut len);

            if res != -1 {
                let addr = addr.assume_init();

                let ip_addr;
                let port;
                if addr.ss_family == libc::AF_INET as u16
                    && len >= mem::size_of::<libc::sockaddr_in>() as i32
                {
                    let addr = &*(&addr as *const _ as *const libc::sockaddr_in);
                    ip_addr = std::net::IpAddr::V4(std::net::Ipv4Addr::from(u32::from_be(
                        addr.sin_addr.s_addr,
                    )));
                    port = u16::from_be(addr.sin_port);
                } else if addr.ss_family == libc::AF_INET6 as u16
                    && len >= mem::size_of::<libc::sockaddr_in6>() as i32
                {
                    let addr = &*(&addr as *const _ as *const libc::sockaddr_in6);
                    ip_addr =
                        std::net::IpAddr::V6(std::net::Ipv6Addr::from(addr.sin6_addr.s6_addr));
                    port = u16::from_be(addr.sin6_port);
                } else {
                    unimplemented!()
                }

                Ok((Socket(res), ip_addr, port))
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    pub fn connect(&mut self, addr: std::net::SocketAddr) -> Result<(), Error> {
        unsafe {
            use std::mem;

            let res = match addr {
                std::net::SocketAddr::V4(addr) => {
                    let addr = libc::sockaddr_in {
                        sin_family: libc::AF_INET as u16,
                        sin_port: u16::to_be(addr.port()),
                        sin_addr: libc::in_addr {
                            s_addr: u32::from_ne_bytes(addr.ip().octets()),
                        },
                        sin_zero: [0; 8],
                    };

                    ffi::srt_connect(
                        self.0,
                        &addr as *const _ as *const _,
                        mem::size_of_val(&addr) as i32,
                    )
                }
                std::net::SocketAddr::V6(addr) => {
                    let addr = libc::sockaddr_in6 {
                        sin6_family: libc::AF_INET6 as u16,
                        sin6_port: u16::to_be(addr.port()),
                        sin6_flowinfo: 0,
                        sin6_addr: libc::in6_addr {
                            s6_addr: addr.ip().octets(),
                        },
                        sin6_scope_id: 0,
                    };

                    ffi::srt_connect(
                        self.0,
                        &addr as *const _ as *const _,
                        mem::size_of_val(&addr) as i32,
                    )
                }
            };

            if res == 0 {
                Ok(())
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    pub fn getpeername(&self) -> Result<(std::net::IpAddr, u16), Error> {
        unsafe {
            use std::mem;

            let mut addr: mem::MaybeUninit<libc::sockaddr_storage> = mem::MaybeUninit::zeroed();
            let res = ffi::srt_getpeername(
                self.0,
                addr.as_mut_ptr() as *mut _,
                mem::size_of_val(&addr) as i32,
            );

            if res != -1 {
                let addr = addr.assume_init();

                let ip_addr;
                let port;
                if addr.ss_family == libc::AF_INET as u16 {
                    let addr = &*(&addr as *const _ as *const libc::sockaddr_in);
                    ip_addr = std::net::IpAddr::V4(std::net::Ipv4Addr::from(addr.sin_addr.s_addr));
                    port = addr.sin_port;
                } else if addr.ss_family == libc::AF_INET6 as u16 {
                    let addr = &*(&addr as *const _ as *const libc::sockaddr_in6);
                    ip_addr =
                        std::net::IpAddr::V6(std::net::Ipv6Addr::from(addr.sin6_addr.s6_addr));
                    port = addr.sin6_port;
                } else {
                    unimplemented!()
                }

                Ok((ip_addr, port))
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    pub fn getsockname(&self) -> Result<(std::net::IpAddr, u16), Error> {
        unsafe {
            use std::mem;

            let mut addr: mem::MaybeUninit<libc::sockaddr_storage> = mem::MaybeUninit::zeroed();
            let res = ffi::srt_getsockname(
                self.0,
                addr.as_mut_ptr() as *mut _,
                mem::size_of_val(&addr) as i32,
            );

            if res != -1 {
                let addr = addr.assume_init();

                let ip_addr;
                let port;
                if addr.ss_family == libc::AF_INET as u16 {
                    let addr = &*(&addr as *const _ as *const libc::sockaddr_in);
                    ip_addr = std::net::IpAddr::V4(std::net::Ipv4Addr::from(addr.sin_addr.s_addr));
                    port = addr.sin_port;
                } else if addr.ss_family == libc::AF_INET6 as u16 {
                    let addr = &*(&addr as *const _ as *const libc::sockaddr_in6);
                    ip_addr =
                        std::net::IpAddr::V6(std::net::Ipv6Addr::from(addr.sin6_addr.s6_addr));
                    port = addr.sin6_port;
                } else {
                    unimplemented!()
                }

                Ok((ip_addr, port))
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    pub fn sendmsg(&mut self, buf: &[u8], ttl: i32, inorder: bool) -> Result<usize, Error> {
        unsafe {
            use std::mem;

            let mut msgctrl: mem::MaybeUninit<ffi::MsgCtrl> = mem::MaybeUninit::uninit();
            ffi::srt_msgctrl_init(msgctrl.as_mut_ptr());
            let mut msgctrl = msgctrl.assume_init();
            msgctrl.msgttl = ttl;
            msgctrl.inorder = if inorder { 1 } else { 0 };

            let res = ffi::srt_sendmsg2(self.0, buf.as_ptr(), buf.len() as i32, &mut msgctrl);

            if res >= 0 {
                Ok(res as usize)
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    pub fn recvmsg(&mut self, buf: &mut [u8]) -> Result<(usize, RecvInfo), Error> {
        unsafe {
            use std::mem;

            let mut msgctrl: mem::MaybeUninit<ffi::MsgCtrl> = mem::MaybeUninit::uninit();
            ffi::srt_msgctrl_init(msgctrl.as_mut_ptr());
            let mut msgctrl = msgctrl.assume_init();

            let res = ffi::srt_recvmsg2(self.0, buf.as_mut_ptr(), buf.len() as i32, &mut msgctrl);

            if res >= 0 {
                Ok((res as usize, RecvInfo(msgctrl)))
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    pub fn reject_reason(&self) -> Result<(), RejectReason> {
        unsafe {
            let res = ffi::srt_getrejectreason(self.0);
            if res == 0 {
                Ok(())
            } else {
                Err(RejectReason::from(res))
            }
        }
    }

    pub fn set_sockopt<'a, S: SockOpt<'a>>(&mut self, value: S::Type) -> Result<(), Error> {
        S::set(self, value)
    }

    pub fn get_sockopt<'a, S: SockOpt<'a>>(&self) -> Result<S::Type, Error> {
        S::get(self)
    }

    pub fn get_sockstate(&self) -> SockStatus {
        unsafe {
            let s = ffi::srt_getsockstate(self.0);
            SockStatus::from(s)
        }
    }
}

pub trait SockOpt<'a> {
    type Type;

    fn set(socket: &mut Socket, value: Self::Type) -> Result<(), Error>;
    fn get(socket: &Socket) -> Result<Self::Type, Error>;
}

macro_rules! impl_sockopt(
    ($name:ident, $opt_name:ident, $t:ty, $ffi_t:ty, $from:expr, $to:expr) => {
        pub enum $name {}
        impl<'a> SockOpt<'a> for $name {
            type Type = $t;

            #[allow(clippy::redundant_closure_call)]
            fn set(socket: &mut Socket, value: Self::Type) -> Result<(), Error> {
                unsafe {
                    use std::mem;

                    let value: $ffi_t = $to(value);

                    let res = ffi::srt_setsockflag(
                        socket.0,
                        ffi::SockOpt::$opt_name,
                        &value as *const _ as *const _,
                        mem::size_of::<$ffi_t>() as i32,
                    );
                    if res >= 0 {
                        Ok(())
                    } else {
                        Err(Error::last_error().expect_err("no error set"))
                    }
                }
            }

            #[allow(clippy::redundant_closure_call)]
            fn get(socket: &Socket) -> Result<Self::Type, Error> {
                unsafe {
                    use std::mem;

                    let mut val: mem::MaybeUninit<$ffi_t> = mem::MaybeUninit::uninit();
                    let mut len = mem::size_of::<$ffi_t>() as i32;

                    let res = ffi::srt_getsockflag(
                        socket.0,
                        ffi::SockOpt::$opt_name,
                        val.as_mut_ptr() as *mut _,
                        &mut len,
                    );

                    assert_eq!(len, mem::size_of::<$ffi_t>() as i32);

                    if res >= 0 {
                        Ok($from(val.assume_init()))
                    } else {
                        Err(Error::last_error().expect_err("no error set"))
                    }
                }
            }
        }
    };
    ($name:ident, $opt_name:ident, $t:ty) => {
        impl_sockopt!($name, $opt_name, $t, $t, |v| v, |v|v);
    };
);

impl_sockopt!(Latency, SRTO_LATENCY, i32);
impl_sockopt!(ReuseAddr, SRTO_REUSEADDR, bool, i32, |v| v != 0, |v| if v {
    1
} else {
    0
});
impl_sockopt!(SndSyn, SRTO_SNDSYN, bool, i32, |v| v != 0, |v| if v {
    1
} else {
    0
});
impl_sockopt!(RcvSyn, SRTO_RCVSYN, bool, i32, |v| v != 0, |v| if v {
    1
} else {
    0
});
impl_sockopt!(
    Rendezvous,
    SRTO_RENDEZVOUS,
    bool,
    i32,
    |v| v != 0,
    |v| if v { 1 } else { 0 }
);
impl_sockopt!(Mss, SRTO_MSS, i32);
impl_sockopt!(Fc, SRTO_FC, i32);
impl_sockopt!(SndBuf, SRTO_SNDBUF, i32);
impl_sockopt!(RcvBuf, SRTO_RCVBUF, i32);
impl_sockopt!(PbKeylen, SRTO_PBKEYLEN, i32);

pub enum Passphrase {}
impl<'a> SockOpt<'a> for Passphrase {
    type Type = Cow<'a, str>;

    #[allow(clippy::redundant_closure_call)]
    fn set(socket: &mut Socket, value: Self::Type) -> Result<(), Error> {
        unsafe {
            use std::mem;

            let res = ffi::srt_setsockflag(
                socket.0,
                ffi::SockOpt::SRTO_PASSPHRASE,
                value.as_ptr() as *const _,
                value.len() as i32,
            );
            if res >= 0 {
                Ok(())
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    #[allow(clippy::redundant_closure_call)]
    fn get(socket: &Socket) -> Result<Self::Type, Error> {
        Err(Error::UNKNOWN)
    }
}

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(i32)]
pub enum RejectReason {
    Unknown = ffi::SRT_REJ_UNKNOWN,
    System = ffi::SRT_REJ_SYSTEM,
    Peer = ffi::SRT_REJ_PEER,
    Resource = ffi::SRT_REJ_RESOURCE,
    Rogue = ffi::SRT_REJ_ROGUE,
    Backlog = ffi::SRT_REJ_BACKLOG,
    IPE = ffi::SRT_REJ_IPE,
    Close = ffi::SRT_REJ_CLOSE,
    Version = ffi::SRT_REJ_VERSION,
    RDVCookie = ffi::SRT_REJ_RDVCOOKIE,
    BadSecret = ffi::SRT_REJ_BADSECRET,
    Unsecure = ffi::SRT_REJ_UNSECURE,
    MessageAPI = ffi::SRT_REJ_MESSAGEAPI,
    Congestion = ffi::SRT_REJ_CONGESTION,
    Filter = ffi::SRT_REJ_FILTER,
    Group = ffi::SRT_REJ_GROUP,
    Timeout = ffi::SRT_REJ_TIMEOUT,
}

impl std::error::Error for RejectReason {}
impl std::fmt::Display for RejectReason {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as std::fmt::Debug>::fmt(self, f)
    }
}

impl From<RejectReason> for i32 {
    fn from(v: RejectReason) -> Self {
        v as i32
    }
}

impl From<i32> for RejectReason {
    fn from(v: i32) -> Self {
        assert_ne!(v, 0);
        match v {
            ffi::SRT_REJ_SYSTEM => RejectReason::System,
            ffi::SRT_REJ_PEER => RejectReason::Peer,
            ffi::SRT_REJ_RESOURCE => RejectReason::Resource,
            ffi::SRT_REJ_ROGUE => RejectReason::Rogue,
            ffi::SRT_REJ_BACKLOG => RejectReason::Backlog,
            ffi::SRT_REJ_IPE => RejectReason::IPE,
            ffi::SRT_REJ_CLOSE => RejectReason::Close,
            ffi::SRT_REJ_VERSION => RejectReason::Version,
            ffi::SRT_REJ_RDVCOOKIE => RejectReason::RDVCookie,
            ffi::SRT_REJ_BADSECRET => RejectReason::BadSecret,
            ffi::SRT_REJ_UNSECURE => RejectReason::Unsecure,
            ffi::SRT_REJ_MESSAGEAPI => RejectReason::MessageAPI,
            ffi::SRT_REJ_CONGESTION => RejectReason::Congestion,
            ffi::SRT_REJ_FILTER => RejectReason::Filter,
            ffi::SRT_REJ_GROUP => RejectReason::Group,
            ffi::SRT_REJ_TIMEOUT => RejectReason::Timeout,
            _ => RejectReason::Unknown,
        }
    }
}

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(i32)]
pub enum SockStatus {
    Init = ffi::SRTS_INIT as i32,
    Opened = ffi::SRTS_OPENED as i32,
    Listening = ffi::SRTS_LISTENING as i32,
    Connecting = ffi::SRTS_CONNECTING as i32,
    Connected = ffi::SRTS_CONNECTED as i32,
    Broken = ffi::SRTS_BROKEN as i32,
    Closing = ffi::SRTS_CLOSING as i32,
    Closed = ffi::SRTS_CLOSED as i32,
    NonExist = ffi::SRTS_NONEXIST as i32,
    Invalid = -1,
}

impl From<u32> for SockStatus {
    fn from(v: u32) -> Self {
        match v {
            ffi::SRTS_INIT => SockStatus::Init,
            ffi::SRTS_OPENED => SockStatus::Opened,
            ffi::SRTS_LISTENING => SockStatus::Listening,
            ffi::SRTS_CONNECTING => SockStatus::Connecting,
            ffi::SRTS_CONNECTED => SockStatus::Connected,
            ffi::SRTS_BROKEN => SockStatus::Broken,
            ffi::SRTS_CLOSING => SockStatus::Closing,
            ffi::SRTS_CLOSED => SockStatus::Closed,
            ffi::SRTS_NONEXIST => SockStatus::NonExist,
            _ => SockStatus::Invalid,
        }
    }
}

impl From<SockStatus> for u32 {
    fn from(v: SockStatus) -> Self {
        v as u32
    }
}

#[derive(Debug)]
pub struct RecvInfo(ffi::MsgCtrl);

unsafe impl Send for RecvInfo {}
unsafe impl Sync for RecvInfo {}

impl RecvInfo {
    pub fn srctime(&self) -> i64 {
        self.0.srctime
    }

    pub fn pkgseq(&self) -> i32 {
        self.0.pktseq
    }

    pub fn msgno(&self) -> i32 {
        self.0.msgno
    }
}

#[derive(Debug)]
pub struct EPoll {
    poll: ffi::EPoll,
    events: [ffi::EPollEvent; 8],
}

unsafe impl Send for EPoll {}

impl EPoll {
    pub fn new() -> Result<EPoll, Error> {
        unsafe {
            let res = ffi::srt_epoll_create();
            if res != -1 {
                Ok(EPoll {
                    poll: res,
                    events: [ffi::EPollEvent { fd: 0, events: 0 }; 8],
                })
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    /// SAFETY: `socket` needs to stay alive at least as long as `self`.
    pub unsafe fn add_usock(&mut self, socket: &Socket, flags: Flags) -> Result<(), Error> {
        unsafe {
            let flags = flags.bits() as i32;
            let res = ffi::srt_epoll_add_usock(self.poll, socket.0, &flags);

            if res == 0 {
                Ok(())
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    /// SAFETY: `socket` must have been added to `self` before.
    pub unsafe fn remove_usock(&mut self, socket: &Socket) -> Result<(), Error> {
        unsafe {
            let res = ffi::srt_epoll_remove_usock(self.poll, socket.0);

            if res == 0 {
                Ok(())
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }

    pub fn uwait(&mut self, ms_timeout: i64) -> Result<&[EPollEvent], Error> {
        unsafe {
            let len = self.events.len() as i32;

            let res = ffi::srt_epoll_uwait(self.poll, self.events.as_mut_ptr(), len, ms_timeout);

            if res > len {
                Err(Error::UNKNOWN)
            } else if res >= 0 {
                Ok(std::slice::from_raw_parts(
                    self.events.as_ptr() as *const _,
                    res as usize,
                ))
            } else {
                Err(Error::last_error().expect_err("no error set"))
            }
        }
    }
}

impl Drop for EPoll {
    fn drop(&mut self) {
        unsafe {
            ffi::srt_epoll_release(self.poll);
        }
    }
}

#[repr(transparent)]
#[derive(Debug)]
pub struct EPollEvent(ffi::EPollEvent);

unsafe impl Send for EPollEvent {}
unsafe impl Sync for EPollEvent {}

impl EPollEvent {
    pub fn socket(&self) -> &Socket {
        unsafe { &*(&self.0.fd as *const _ as *const Socket) }
    }

    pub fn events(&self) -> Flags {
        Flags::from_bits_truncate(self.0.events)
    }
}

bitflags::bitflags! {
    pub struct Flags: u32 {
        const IN = ffi::EPOLL_OPT_IN;
        const OUT = ffi::EPOLL_OPT_OUT;
        const ERR = ffi::EPOLL_OPT_ERR;
        const UPDATE = ffi::EPOLL_OPT_UPDATE;
        const ET = ffi::EPOLL_OPT_ET;
    }
}
