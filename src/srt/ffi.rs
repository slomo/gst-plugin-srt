// Copyright (C) 2022 Sebastian Dröge <sebastian@centricular.com>
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0
#![allow(unused)]

pub type Socket = i32;
pub type SockStatus = u32;
pub type MemberStatus = u32;
pub type EPoll = i32;
pub type RejectReason = i32;

pub const SRTS_INIT: SockStatus = 1;
pub const SRTS_OPENED: SockStatus = 2;
pub const SRTS_LISTENING: SockStatus = 3;
pub const SRTS_CONNECTING: SockStatus = 4;
pub const SRTS_CONNECTED: SockStatus = 5;
pub const SRTS_BROKEN: SockStatus = 6;
pub const SRTS_CLOSING: SockStatus = 7;
pub const SRTS_CLOSED: SockStatus = 8;
pub const SRTS_NONEXIST: SockStatus = 9;

pub const MEMBER_STATUS_PENDING: MemberStatus = 0;
pub const MEMBER_STATUS_IDLE: MemberStatus = 1;
pub const MEMBER_STATUS_RUNNING: MemberStatus = 2;
pub const MEMBER_STATUS_BROKEN: MemberStatus = 3;

pub const TRANSTYPE_LIVE: u32 = 0;
pub const TRANSTYPE_FILE: u32 = 1;

pub const EUNKNOWN: i32 = -1;
pub const SUCCESS: i32 = 0;
pub const ECONNSETUP: i32 = 1000;
pub const ENOSERVER: i32 = 1001;
pub const ECONNREJ: i32 = 1002;
pub const ESOCKFAIL: i32 = 1003;
pub const ESECFAIL: i32 = 1004;
pub const ESCLOSED: i32 = 1005;
pub const ECONNFAIL: i32 = 2000;
pub const ECONNLOST: i32 = 2001;
pub const ENOCONN: i32 = 2002;
pub const ERESOURCE: i32 = 3000;
pub const ETHREAD: i32 = 3001;
pub const ENOBUF: i32 = 3002;
pub const ESYSOBJ: i32 = 3003;
pub const EFILE: i32 = 4000;
pub const EINVRDOFF: i32 = 4001;
pub const ERDPERM: i32 = 4002;
pub const EINVWROFF: i32 = 4003;
pub const EWRPERM: i32 = 4004;
pub const EINVOP: i32 = 5000;
pub const EBOUNDSOCK: i32 = 5001;
pub const ECONNSOCK: i32 = 5002;
pub const EINVPARAM: i32 = 5003;
pub const EINVSOCK: i32 = 5004;
pub const EUNBOUNDSOCK: i32 = 5005;
pub const ENOLISTEN: i32 = 5006;
pub const ERDVNOSERV: i32 = 5007;
pub const ERDVUNBOUND: i32 = 5008;
pub const EINVALMSGAPI: i32 = 5009;
pub const EINVALBUFFERAPI: i32 = 5010;
pub const EDUPLISTEN: i32 = 5011;
pub const ELARGEMSG: i32 = 5012;
pub const EINVPOLLID: i32 = 5013;
pub const EPOLLEMPTY: i32 = 5014;
pub const EBINDCONFLICT: i32 = 5016;
pub const EASYNCFAIL: i32 = 6000;
pub const EASYNCSND: i32 = 6001;
pub const EASYNCRCV: i32 = 6002;
pub const ETIMEOUT: i32 = 6003;
pub const ECONGEST: i32 = 6004;
pub const EPEERERR: i32 = 7000;

pub const EPOLL_OPT_IN: u32 = 0x1;
pub const EPOLL_OPT_OUT: u32 = 0x4;
pub const EPOLL_OPT_ERR: u32 = 0x8;
pub const EPOLL_OPT_UPDATE: u32 = 0x10;
pub const EPOLL_OPT_ET: u32 = 0x8000_0000;

pub const SRT_REJ_UNKNOWN: i32 = 0; // initial set when in progress
pub const SRT_REJ_SYSTEM: i32 = 1; // broken due to system function error
pub const SRT_REJ_PEER: i32 = 2; // connection was rejected by peer
pub const SRT_REJ_RESOURCE: i32 = 3; // internal problem with resource allocation
pub const SRT_REJ_ROGUE: i32 = 4; // incorrect data in handshake messages
pub const SRT_REJ_BACKLOG: i32 = 5; // listener's backlog exceeded
pub const SRT_REJ_IPE: i32 = 6; // internal program error
pub const SRT_REJ_CLOSE: i32 = 7; // socket is closing
pub const SRT_REJ_VERSION: i32 = 8; // peer is older version than agent's minimum set
pub const SRT_REJ_RDVCOOKIE: i32 = 9; // rendezvous cookie collision
pub const SRT_REJ_BADSECRET: i32 = 10; // wrong password
pub const SRT_REJ_UNSECURE: i32 = 11; // password required or unexpected
pub const SRT_REJ_MESSAGEAPI: i32 = 12; // streamapi/messageapi collision
pub const SRT_REJ_CONGESTION: i32 = 13; // incompatible congestion-controller type
pub const SRT_REJ_FILTER: i32 = 14; // incompatible packet filter
pub const SRT_REJ_GROUP: i32 = 15; // incompatible group
pub const SRT_REJ_TIMEOUT: i32 = 16; // connection timeout

#[allow(non_camel_case_types)]
#[repr(C)]
pub enum SockOpt {
    SRTO_MSS = 0,           // the Maximum Transfer Unit
    SRTO_SNDSYN = 1,        // if sending is blocking
    SRTO_RCVSYN = 2,        // if receiving is blocking
    SRTO_ISN = 3, // Initial Sequence Number (valid only after srt_connect or srt_accept-ed sockets)
    SRTO_FC = 4,  // Flight flag size (window size)
    SRTO_SNDBUF = 5, // maximum buffer in sending queue
    SRTO_RCVBUF = 6, // UDT receiving buffer size
    SRTO_LINGER = 7, // waiting for unsent data when closing
    SRTO_UDP_SNDBUF = 8, // UDP sending buffer size
    SRTO_UDP_RCVBUF = 9, // UDP receiving buffer size
    SRTO_RENDEZVOUS = 12, // rendezvous connection mode
    SRTO_SNDTIMEO = 13, // send() timeout
    SRTO_RCVTIMEO = 14, // recv() timeout
    SRTO_REUSEADDR = 15, // reuse an existing port or create a new one
    SRTO_MAXBW = 16, // maximum bandwidth (bytes per second) that the connection can use
    SRTO_STATE = 17, // current socket state, see UDTSTATUS, read only
    SRTO_EVENT = 18, // current available events associated with the socket
    SRTO_SNDDATA = 19, // size of data in the sending buffer
    SRTO_RCVDATA = 20, // size of data available for recv
    SRTO_SENDER = 21, // Sender mode (independent of conn mode), for encryption, tsbpd handshake.
    SRTO_TSBPDMODE = 22, // Enable/Disable TsbPd. Enable -> Tx set origin timestamp, Rx deliver packet at origin time + delay
    SRTO_LATENCY = 23, // NOT RECOMMENDED. SET: to both SRTO_RCVLATENCY and SRTO_PEERLATENCY. GET: same as SRTO_RCVLATENCY.
    SRTO_INPUTBW = 24, // Estimated input stream rate.
    SRTO_OHEADBW, // MaxBW ceiling based on % over input stream rate. Applies when UDT_MAXBW=0 (auto).
    SRTO_PASSPHRASE = 26, // Crypto PBKDF2 Passphrase (must be 10..79 characters, or empty to disable encryption)
    SRTO_PBKEYLEN,        // Crypto key len in bytes {16,24,32} Default: 16 (AES-128)
    SRTO_KMSTATE,         // Key Material exchange status (UDT_SRTKmState)
    SRTO_IPTTL = 29,      // IP Time To Live (passthru for system sockopt IPPROTO_IP/IP_TTL)
    SRTO_IPTOS,           // IP Type of Service (passthru for system sockopt IPPROTO_IP/IP_TOS)
    SRTO_TLPKTDROP = 31,  // Enable receiver pkt drop
    SRTO_SNDDROPDELAY = 32, // Extra delay towards latency for sender TLPKTDROP decision (-1 to off)
    SRTO_NAKREPORT = 33,  // Enable receiver to send periodic NAK reports
    SRTO_VERSION = 34,    // Local SRT Version
    SRTO_PEERVERSION,     // Peer SRT Version (from SRT Handshake)
    SRTO_CONNTIMEO = 36,  // Connect timeout in msec. Caller default: 3000, rendezvous (x 10)
    SRTO_DRIFTTRACER = 37, // Enable or disable drift tracer
    SRTO_MININPUTBW = 38, // Minimum estimate of input stream rate.
    // (some space left)
    SRTO_SNDKMSTATE = 40, // (GET) the current state of the encryption at the peer side
    SRTO_RCVKMSTATE,      // (GET) the current state of the encryption at the agent side
    SRTO_LOSSMAXTTL, // Maximum possible packet reorder tolerance (number of packets to receive after loss to send lossreport)
    SRTO_RCVLATENCY, // TsbPd receiver delay (mSec) to absorb burst of missed packet retransmission
    SRTO_PEERLATENCY, // Minimum value of the TsbPd receiver delay (mSec) for the opposite side (peer)
    SRTO_MINVERSION, // Minimum SRT version needed for the peer (peers with less version will get connection reject)
    SRTO_STREAMID,   // A string set to a socket and passed to the listener's accepted socket
    SRTO_CONGESTION, // Congestion controller type selection
    SRTO_MESSAGEAPI, // In File mode, use message API (portions of data with boundaries)
    SRTO_PAYLOADSIZE, // Maximum payload size sent in one UDP packet (0 if unlimited)
    SRTO_TRANSTYPE = 50, // Transmission type (set of options required for given transmission type)
    SRTO_KMREFRESHRATE, // After sending how many packets the encryption key should be flipped to the new key
    SRTO_KMPREANNOUNCE, // How many packets before key flip the new key is annnounced and after key flip the old one decommissioned
    SRTO_ENFORCEDENCRYPTION, // Connection to be rejected or quickly broken when one side encryption set or bad password
    SRTO_IPV6ONLY,           // IPV6_V6ONLY mode
    SRTO_PEERIDLETIMEO,      // Peer-idle timeout (max time of silence heard from peer) in [ms]
    SRTO_BINDTODEVICE, // Forward the SOL_SOCKET/SO_BINDTODEVICE option on socket (pass packets only from that device)
    SRTO_PACKETFILTER = 60, // Add and configure a packet filter
    SRTO_RETRANSMITALGO = 61, // An option to select packet retransmission algorithm
}

#[repr(C)]
#[derive(Debug)]
pub struct MsgCtrl {
    pub(super) flags: u32,
    pub(super) msgttl: i32,
    pub(super) inorder: i32,
    pub(super) boundary: i32,
    pub(super) srctime: i64,
    pub(super) pktseq: i32,
    pub(super) msgno: i32,
    pub(super) grpdata: *const GroupData,
    pub(super) grpdata_size: usize,
}

#[repr(C)]
pub struct GroupData {
    id: Socket,
    peeraddr: libc::sockaddr_storage,
    sockstate: SockStatus,
    weight: u16,
    memberstate: MemberStatus,
    result: i32,
    token: i32,
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct EPollEvent {
    pub(super) fd: Socket,
    pub(super) events: u32,
}

extern "C" {
    pub fn srt_startup() -> i32;
    pub fn srt_cleanup() -> i32;
    pub fn srt_getversion() -> u32;

    pub fn srt_time_now() -> i64;

    pub fn srt_create_socket() -> Socket;
    pub fn srt_close(socket: Socket);

    pub fn srt_bind(socket: Socket, name: *const libc::sockaddr, namelen: i32) -> i32;
    pub fn srt_listen(socket: Socket, backlog: i32) -> i32;
    pub fn srt_accept(socket: Socket, name: *mut libc::sockaddr, namelen: *mut i32) -> i32;
    pub fn srt_connect(socket: Socket, name: *const libc::sockaddr, namelen: i32) -> i32;

    pub fn srt_getpeername(socket: Socket, name: *mut libc::sockaddr, namelen: i32) -> i32;
    pub fn srt_getsockname(socket: Socket, name: *mut libc::sockaddr, namelen: i32) -> i32;
    pub fn srt_getsockflag(
        socket: Socket,
        opt: SockOpt,
        optval: *mut libc::c_void,
        optlen: *mut i32,
    ) -> i32;
    pub fn srt_setsockflag(
        socket: Socket,
        opt: SockOpt,
        optval: *const libc::c_void,
        optlen: i32,
    ) -> i32;
    pub fn srt_getsockstate(socket: Socket) -> SockStatus;
    pub fn srt_getrejectreason(socket: Socket) -> RejectReason;

    pub fn srt_sendmsg2(socket: Socket, buf: *const u8, len: i32, mctrl: *mut MsgCtrl) -> i32;
    pub fn srt_recvmsg2(socket: Socket, buf: *mut u8, len: i32, mctrl: *mut MsgCtrl) -> i32;

    pub fn srt_epoll_create() -> EPoll;
    pub fn srt_epoll_release(epoll: EPoll);
    pub fn srt_epoll_add_usock(epoll: EPoll, socket: Socket, events: *const i32) -> i32;
    pub fn srt_epoll_remove_usock(epoll: EPoll, socket: Socket) -> i32;
    pub fn srt_epoll_uwait(
        epoll: EPoll,
        fds_set: *mut EPollEvent,
        fds_size: i32,
        ms_timeout: i64,
    ) -> i32;

    pub fn srt_getlasterror(errno: *mut i32) -> i32;
    pub fn srt_getlasterror_str() -> *const libc::c_char;
    pub fn srt_clearlasterror();
    pub fn srt_strerror(code: i32, errnoval: i32) -> *const libc::c_char;

    pub fn srt_msgctrl_init(msgctrl: *mut MsgCtrl);

    pub fn srt_setloglevel(level: i32);
    pub fn srt_setlogflags(flags: u32);
    pub fn srt_setloghandler(
        opaque: *mut libc::c_void,
        handler: Option<
            unsafe extern "C" fn(
                *mut libc::c_void,
                i32,
                *const libc::c_char,
                i32,
                *const libc::c_char,
                *const libc::c_char,
            ),
        >,
    );
}
