// Copyright (C) 2022 Sebastian Dröge <sebastian@centricular.com>
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0

use gst::glib;
use gst::prelude::*;
use gst::subclass::prelude::*;
use gst_base::prelude::*;
use gst_base::subclass::base_src::CreateSuccess;
use gst_base::subclass::prelude::*;

use std::collections::VecDeque;
use std::sync::{atomic, Arc, Condvar, Mutex};

use once_cell::sync::Lazy;

use smallvec::SmallVec;

use crate::srt;

static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "rssrtsrc",
        gst::DebugColorFlags::empty(),
        Some("Rust SRT Source"),
    )
});

#[derive(Debug, Clone)]
struct Settings {
    location: Option<String>,
    queue_size: u32,
    auto_reconnect: bool,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            location: None,
            queue_size: 1024,
            auto_reconnect: true,
        }
    }
}

struct State {
    receiver: Receiver,
    latency: u32,
    next_seqno: Option<i32>,
}

impl Drop for State {
    fn drop(&mut self) {
        self.receiver.shutdown();
    }
}

#[derive(Default)]
pub struct SrtSrc {
    settings: Mutex<Settings>,
    state: Mutex<Option<State>>,
}

impl SrtSrc {}

#[glib::object_subclass]
impl ObjectSubclass for SrtSrc {
    const NAME: &'static str = "RsSrtSrc";
    type Type = super::SrtSrc;
    type ParentType = gst_base::PushSrc;
}

impl ObjectImpl for SrtSrc {
    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
            vec![
                glib::ParamSpecString::builder("location")
                    .nick("Location")
                    .blurb("URL for the SRT stream to play")
                    .mutable_ready()
                    .build(),
                glib::ParamSpecUInt::builder("queue-size")
                    .nick("Queue Size")
                    .blurb("Queue size")
                    .default_value(Settings::default().queue_size)
                    .mutable_ready()
                    .build(),
                glib::ParamSpecBoolean::builder("auto-reconnect")
                    .nick("Auto Reconnect")
                    .blurb("Automatically try reconnecting on errors")
                    .default_value(Settings::default().auto_reconnect)
                    .mutable_ready()
                    .build(),
            ]
        });

        PROPERTIES.as_ref()
    }

    fn constructed(&self) {
        self.parent_constructed();

        let obj = self.obj();
        obj.set_live(true);
        obj.set_format(gst::Format::Time);
        obj.set_blocksize(1316);
    }

    fn set_property(&self, _id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
        match pspec.name() {
            "location" => {
                let mut settings = self.settings.lock().unwrap();
                let location = value
                    .get::<Option<String>>()
                    .expect("type checked upstream");

                let location = match location {
                    Some(location) => location,
                    None => {
                        gst::info!(
                            CAT,
                            imp = self,
                            "Changing URI from {} to NONE",
                            settings.location.as_deref().unwrap_or("NONE"),
                        );
                        settings.location = None;
                        return;
                    }
                };

                gst::info!(
                    CAT,
                    imp = self,
                    "Changing URI from {} to {}",
                    settings.location.as_deref().unwrap_or("NONE"),
                    location
                );
                settings.location = Some(location);
            }
            "queue-size" => {
                let mut settings = self.settings.lock().unwrap();
                let queue_size = value.get().expect("type checked upstream");
                gst::info!(
                    CAT,
                    imp = self,
                    "Changing queue-size from {} to {}",
                    settings.queue_size,
                    queue_size
                );
                settings.queue_size = queue_size;
            }
            "auto-reconnect" => {
                let mut settings = self.settings.lock().unwrap();
                let auto_reconnect = value.get().expect("type checked upstream");
                gst::info!(
                    CAT,
                    imp = self,
                    "Changing auto-reconnect from {} to {}",
                    settings.auto_reconnect,
                    auto_reconnect
                );
                settings.auto_reconnect = auto_reconnect;
            }
            _ => unimplemented!(),
        }
    }

    fn property(&self, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            "location" => {
                let settings = self.settings.lock().unwrap();
                settings.location.to_value()
            }
            "queue-size" => {
                let settings = self.settings.lock().unwrap();
                settings.queue_size.to_value()
            }
            "auto-reconnect" => {
                let settings = self.settings.lock().unwrap();
                settings.auto_reconnect.to_value()
            }
            _ => unimplemented!(),
        }
    }
}

impl GstObjectImpl for SrtSrc {}

impl ElementImpl for SrtSrc {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "SRT Source",
                "Source/Network",
                "Reads an SRT stream",
                "Sebastian Dröge <sebastian@centricular.com>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }

    fn pad_templates() -> &'static [gst::PadTemplate] {
        static PAD_TEMPLATES: Lazy<Vec<gst::PadTemplate>> = Lazy::new(|| {
            let caps = gst::Caps::new_any();
            let src_pad_template = gst::PadTemplate::new(
                "src",
                gst::PadDirection::Src,
                gst::PadPresence::Always,
                &caps,
            )
            .unwrap();

            vec![src_pad_template]
        });

        PAD_TEMPLATES.as_ref()
    }
}

impl BaseSrcImpl for SrtSrc {
    fn start(&self) -> Result<(), gst::ErrorMessage> {
        if let Err(_err) = srt::startup() {
            return Err(gst::error_msg!(
                gst::LibraryError::Init,
                ["Failed to initialize SRT"]
            ));
        }

        let settings = self.settings.lock().unwrap();

        let location = match settings.location {
            Some(ref location) => location,
            None => {
                return Err(gst::error_msg!(
                    gst::ResourceError::OpenRead,
                    ["No location set"]
                ));
            }
        };

        let (receiver, latency) = Receiver::new(
            &self.obj(),
            location,
            settings.queue_size as usize,
            self.obj().blocksize() as usize,
        )?;
        *self.state.lock().unwrap() = Some(State {
            receiver,
            latency,
            next_seqno: None,
        });

        gst::info!(CAT, imp = self, "Started");

        Ok(())
    }

    fn stop(&self) -> Result<(), gst::ErrorMessage> {
        *self.state.lock().unwrap() = None;
        self.unlock()?;

        if let Err(_err) = srt::shutdown() {
            return Err(gst::error_msg!(
                gst::LibraryError::Init,
                ["Failed to shutdown SRT"]
            ));
        }

        gst::info!(CAT, imp = self, "Stopped");

        Ok(())
    }

    fn query(&self, query: &mut gst::QueryRef) -> bool {
        use gst::QueryViewMut;

        match query.view_mut() {
            QueryViewMut::Latency(q) => {
                let state = self.state.lock().unwrap();

                if let Some(ref state) = &*state {
                    let latency = gst::ClockTime::from_mseconds(state.latency as u64);
                    gst::debug!(CAT, imp = self, "Returning latency {}", latency);
                    q.set(true, latency, gst::ClockTime::NONE);
                    true
                } else {
                    false
                }
            }
            _ => BaseSrcImplExt::parent_query(self, query),
        }
    }

    fn is_seekable(&self) -> bool {
        false
    }

    fn unlock(&self) -> Result<(), gst::ErrorMessage> {
        gst::debug!(CAT, imp = self, "Unlocking");
        let state_guard = self.state.lock().unwrap();
        if let Some(ref state) = &*state_guard {
            let receiver = state.receiver.clone();
            drop(state_guard);
            receiver.set_flushing(true);
        }
        gst::debug!(CAT, imp = self, "Unlocked");

        Ok(())
    }

    fn unlock_stop(&self) -> Result<(), gst::ErrorMessage> {
        gst::debug!(CAT, imp = self, "Stopping unlocking");

        let state_guard = self.state.lock().unwrap();
        if let Some(ref state) = &*state_guard {
            let receiver = state.receiver.clone();
            drop(state_guard);
            receiver.set_flushing(false);
        }

        gst::debug!(CAT, imp = self, "Stopped unlocking");

        Ok(())
    }
}

impl PushSrcImpl for SrtSrc {
    fn create(
        &self,
        _buffer: Option<&mut gst::BufferRef>,
    ) -> Result<CreateSuccess, gst::FlowError> {
        'retry: loop {
            let auto_reconnect = self.settings.lock().unwrap().auto_reconnect;
            let mut state_guard = self.state.lock().unwrap();
            let mut state = match *state_guard {
                None => {
                    gst::error!(CAT, imp = self, "Not started yet");
                    gst::element_imp_error!(self, gst::CoreError::Negotiation, ["Not started yet"]);
                    return Err(gst::FlowError::Error);
                }
                Some(ref mut state) => state,
            };

            let receiver = state.receiver.clone();
            drop(state_guard);

            let items = match receiver.dequeue() {
                Some(items) => items,
                None => {
                    gst::debug!(CAT, imp = self, "Flushing");
                    return Err(gst::FlowError::Flushing);
                }
            };

            state_guard = self.state.lock().unwrap();
            state = match *state_guard {
                Some(ref mut state) => state,
                None => {
                    gst::debug!(CAT, imp = self, "Flushing");
                    return Err(gst::FlowError::Flushing);
                }
            };

            let mut list = (items.len() > 1).then(|| gst::BufferList::new_sized(items.len()));

            for item in items {
                match item {
                    QueueItem::Data {
                        mut buffer,
                        seqno,
                        msgno,
                        srctime,
                    } => {
                        gst::trace!(
                            CAT,
                            imp = self,
                            "Received buffer of {} bytes for message with seqno {seqno}, msgno {msgno}, srctime {srctime}", buffer.size(),
                        );

                        let gst_now = {
                            let clock = self.obj().clock();
                            let base_time = self.obj().base_time();

                            Option::zip(clock, base_time).and_then(|(clock, base_time)| {
                                clock.time().opt_saturating_sub(base_time)
                            })
                        };
                        let srt_now = gst::ClockTime::from_useconds(srt::time_now() as u64);
                        let srctime =
                            (srctime != 0).then(|| gst::ClockTime::from_useconds(srctime as u64));

                        let discont = match state.next_seqno {
                            Some(next_seqno) => {
                                if next_seqno != seqno {
                                    gst::warning!(
                                        CAT,
                                        imp = self,
                                        "Expected seqno {next_seqno}, got seqno {seqno}"
                                    );
                                    true
                                } else {
                                    false
                                }
                            }
                            None => true,
                        };
                        state.next_seqno = Some(if seqno == i32::MAX { 0 } else { seqno + 1 });

                        let dts = gst_now.and_then(|gst_now| {
                            if let Some(srctime) = srctime {
                                if srctime > srt_now {
                                    let diff = srctime - srt_now;

                                    gst_now.checked_add(diff)
                                } else {
                                    let diff = srt_now - srctime;

                                    Some(gst_now.saturating_sub(diff))
                                }
                            } else {
                                Some(gst_now)
                            }
                        });

                        gst::trace!(
                            CAT,
                            imp = self,
                            "SRT now {}, GStreamer now {}, srctime {}",
                            srt_now,
                            gst_now.display(),
                            srctime.display()
                        );

                        {
                            let buffer = buffer.get_mut().unwrap();
                            buffer.set_dts(dts);

                            if discont {
                                buffer.set_flags(gst::BufferFlags::DISCONT);
                            }
                        }
                        if let Some(ref mut list) = list {
                            gst::trace!(CAT, imp = self, "Outputting buffer {buffer:?}");
                            list.get_mut().unwrap().add(buffer);
                        } else {
                            return Ok(CreateSuccess::NewBuffer(buffer));
                        }
                    }
                    QueueItem::Error(err) => {
                        gst::error!(CAT, imp = self, "Stream error: {}", err);
                        if auto_reconnect {
                            state.receiver.restart();
                            continue 'retry;
                        } else {
                            gst::element_imp_error!(
                                self,
                                gst::StreamError::Failed,
                                ["Stream error: {}", err]
                            );
                            return Err(gst::FlowError::Error);
                        }
                    }
                    QueueItem::Eos => {
                        gst::debug!(CAT, imp = self, "EOS");
                        return Err(gst::FlowError::Eos);
                    }
                }
            }

            let list = list.unwrap();

            return Ok(CreateSuccess::NewBufferList(list));
        }
    }
}

#[derive(Clone)]
struct Receiver(Arc<ReceiverInner>);

struct ReceiverInner {
    element: glib::WeakRef<super::SrtSrc>,
    queue: Mutex<ReceiverQueue>,
    queue_cond: Condvar,
    shutdown: atomic::AtomicBool,
    buffer_pool: gst::BufferPool,
    parsed_uri: Mutex<ParsedUri>,
    join_handle: Mutex<Option<std::thread::JoinHandle<()>>>,
}

struct ReceiverQueue {
    queue: VecDeque<QueueItem>,
    queue_len: usize,
    flushing: bool,
}

#[derive(Debug)]
enum QueueItem {
    Data {
        buffer: gst::Buffer,
        seqno: i32,
        msgno: i32,
        srctime: i64,
    },
    Error(srt::Error),
    Eos,
}

impl Receiver {
    fn shutdown(&self) {
        let element = match self.0.element.upgrade() {
            Some(element) => element,
            None => return,
        };

        gst::debug!(CAT, obj = &element, "Shutting down");

        self.0.shutdown.store(true, atomic::Ordering::SeqCst);
    }

    fn restart(&self) {
        let element = match self.0.element.upgrade() {
            Some(element) => element,
            None => return,
        };

        gst::debug!(CAT, obj = &element, "Restarting");

        self.0.shutdown.store(true, atomic::Ordering::SeqCst);

        gst::debug!(CAT, obj = &element, "Waiting for receive thread");
        let join_handle = self.0.join_handle.lock().unwrap().take();
        if let Some(join_handle) = join_handle {
            let _ = join_handle.join();
        }
        gst::debug!(CAT, obj = &element, "Waited for receive thread");

        {
            // Resolve hostname again if any, it might've changed
            let mut parsed_uri = self.0.parsed_uri.lock().unwrap();
            if let Some(ref host) = parsed_uri.host {
                use std::net::ToSocketAddrs;

                let res = (host.as_str(), parsed_uri.socket_addr.port())
                    .to_socket_addrs()
                    .and_then(|mut it| {
                        it.next()
                            .ok_or_else(|| std::io::Error::from(std::io::ErrorKind::NotFound))
                    });
                match res {
                    Ok(socket_addr) => {
                        if parsed_uri.socket_addr != socket_addr {
                            gst::debug!(CAT, obj = &element, "Connecting to {socket_addr}");
                            parsed_uri.socket_addr = socket_addr;
                        }
                    }
                    Err(err) => {
                        gst::error!(CAT, obj = &element, "Can't resolve {}: {}", host, err)
                    }
                }
            }
        }

        self.0.shutdown.store(false, atomic::Ordering::SeqCst);

        let self_clone = self.clone();
        let join_handle = std::thread::spawn(move || Self::run(self_clone));
        *self.0.join_handle.lock().unwrap() = Some(join_handle);
    }

    fn enqueue(&self, item: QueueItem) {
        let element = match self.0.element.upgrade() {
            Some(element) => element,
            None => return,
        };

        let mut inner = self.0.queue.lock().unwrap();
        if inner.flushing {
            gst::trace!(CAT, obj = &element, "Flushing");
            return;
        }

        while inner.queue.len() > inner.queue_len {
            let _ = inner.queue.pop_front();
            gst::warning!(CAT, obj = &element, "Dropping old buffer");
        }

        gst::trace!(CAT, obj = &element, "Queueing {item:?}");
        inner.queue.push_back(item);
        self.0.queue_cond.notify_one();
    }

    fn dequeue(&self) -> Option<SmallVec<[QueueItem; 16]>> {
        let element = match self.0.element.upgrade() {
            Some(element) => element,
            None => return None,
        };

        gst::trace!(CAT, obj = &element, "Dequeueing item");

        let mut inner = self.0.queue.lock().unwrap();

        while !inner.flushing && inner.queue.is_empty() {
            inner = self.0.queue_cond.wait(inner).unwrap();
        }

        if inner.flushing {
            gst::trace!(CAT, obj = &element, "Flushing");
            None
        } else {
            let mut items = SmallVec::with_capacity(inner.queue.len());

            // Pop all data items in one go
            while inner
                .queue
                .front()
                .map_or(false, |item| matches!(item, QueueItem::Data { .. }))
            {
                items.push(inner.queue.pop_front().unwrap());
            }

            // If nothing was popped above then the top item is not a data item and we have to
            // return it separately
            if items.is_empty() {
                items.push(inner.queue.pop_front().unwrap());
            }

            gst::trace!(CAT, obj = &element, "Dequeued {items:?}");
            Some(items)
        }
    }

    fn set_flushing(&self, flushing: bool) {
        let element = match self.0.element.upgrade() {
            Some(element) => element,
            None => return,
        };

        gst::debug!(CAT, obj = &element, "Flushing {flushing}");

        let mut inner = self.0.queue.lock().unwrap();
        if flushing {
            inner.flushing = true;
            inner.queue.clear();
            self.0.queue_cond.notify_one();
        } else {
            inner.flushing = false;
        }
    }

    fn new(
        element: &super::SrtSrc,
        location: &str,
        queue_size: usize,
        blocksize: usize,
    ) -> Result<(Receiver, u32), gst::ErrorMessage> {
        let parsed_uri = location.parse::<ParsedUri>()?;

        gst::debug!(
            CAT,
            obj = &element,
            "Connecting to {}",
            parsed_uri.socket_addr
        );

        let buffer_pool = gst::BufferPool::new();
        let mut config = buffer_pool.config();
        config.set_params(None, blocksize as u32, 100, 0);
        buffer_pool.set_config(config).map_err(|err| {
            gst::error_msg!(
                gst::ResourceError::OpenRead,
                ["Failed to configure buffer pool: {err}"]
            )
        })?;
        buffer_pool.set_active(true).map_err(|err| {
            gst::error_msg!(
                gst::ResourceError::OpenRead,
                ["Failed to start buffer pool: {err}"]
            )
        })?;

        let latency = parsed_uri.latency;
        let receiver = Receiver(Arc::new(ReceiverInner {
            element: element.downgrade(),
            queue: Mutex::new(ReceiverQueue {
                queue: VecDeque::new(),
                queue_len: queue_size,
                flushing: false,
            }),
            queue_cond: Condvar::new(),
            shutdown: atomic::AtomicBool::new(false),
            buffer_pool,
            parsed_uri: Mutex::new(parsed_uri),
            join_handle: Mutex::default(),
        }));
        let receiver_clone = receiver.clone();

        let join_handle = std::thread::spawn(move || Self::run(receiver));
        *receiver_clone.0.join_handle.lock().unwrap() = Some(join_handle);

        Ok((receiver_clone, latency))
    }

    fn setup(
        element: &super::SrtSrc,
        receiver: &Receiver,
    ) -> Result<Option<Poll>, gst::ErrorMessage> {
        let parsed_uri = receiver.0.parsed_uri.lock().unwrap().clone();

        let mode = parsed_uri
            .parameters
            .iter()
            .find(|(key, _value)| key == "mode")
            .map(|(_key, value)| value.parse::<Mode>())
            .transpose()?
            .unwrap_or_else(|| {
                if parsed_uri.socket_addr.ip().is_unspecified() {
                    Mode::Listener
                } else {
                    Mode::Caller
                }
            });

        let mut poll = Poll::new().map_err(|err| {
            gst::error_msg!(
                gst::ResourceError::OpenRead,
                ["Failed to create epoll: {}", err]
            )
        })?;

        let mut socket = srt::Socket::new().map_err(|err| {
            gst::error_msg!(
                gst::ResourceError::OpenRead,
                ["Failed to create socket: {}", err]
            )
        })?;

        socket
            .set_sockopt::<srt::Latency>(parsed_uri.latency as i32)
            .map_err(|err| {
                gst::error_msg!(
                    gst::ResourceError::OpenRead,
                    ["Failed to set latency: {}", err]
                )
            })?;

        for (key, value) in &parsed_uri.parameters {
            if ["latency", "mode"].contains(&key.as_str()) {
                continue;
            }

            match key.as_str() {
                "mss" => {
                    let v = match value.parse::<i32>() {
                        Ok(v) => v,
                        Err(err) => {
                            gst::warning!(
                                CAT,
                                obj = element,
                                "Failed to parse '{}' value '{}': {}",
                                key,
                                value,
                                err
                            );
                            continue;
                        }
                    };

                    if let Err(err) = socket.set_sockopt::<srt::Mss>(v) {
                        gst::warning!(
                            CAT,
                            obj = element,
                            "Failed to set '{}' sockopt value '{}': {}",
                            key,
                            value,
                            err
                        );
                    }
                }
                "fc" => {
                    let v = match value.parse::<i32>() {
                        Ok(v) => v,
                        Err(err) => {
                            gst::warning!(
                                CAT,
                                obj = element,
                                "Failed to parse '{}' value '{}': {}",
                                key,
                                value,
                                err
                            );
                            continue;
                        }
                    };

                    if let Err(err) = socket.set_sockopt::<srt::Fc>(v) {
                        gst::warning!(
                            CAT,
                            obj = element,
                            "Failed to set '{}' sockopt value '{}': {}",
                            key,
                            value,
                            err
                        );
                    }
                }
                "sndbuf" => {
                    let v = match value.parse::<i32>() {
                        Ok(v) => v,
                        Err(err) => {
                            gst::warning!(
                                CAT,
                                obj = element,
                                "Failed to parse '{}' value '{}': {}",
                                key,
                                value,
                                err
                            );
                            continue;
                        }
                    };

                    if let Err(err) = socket.set_sockopt::<srt::SndBuf>(v) {
                        gst::warning!(
                            CAT,
                            obj = element,
                            "Failed to set '{}' sockopt value '{}': {}",
                            key,
                            value,
                            err
                        );
                    }
                }
                "rcvbuf" => {
                    let v = match value.parse::<i32>() {
                        Ok(v) => v,
                        Err(err) => {
                            gst::warning!(
                                CAT,
                                obj = element,
                                "Failed to parse '{}' value '{}': {}",
                                key,
                                value,
                                err
                            );
                            continue;
                        }
                    };

                    if let Err(err) = socket.set_sockopt::<srt::RcvBuf>(v) {
                        gst::warning!(
                            CAT,
                            obj = element,
                            "Failed to set '{}' sockopt value '{}': {}",
                            key,
                            value,
                            err
                        );
                    }
                }
                "passphrase" => {
                    if let Err(err) = socket.set_sockopt::<srt::Passphrase>(value.as_str().into()) {
                        gst::warning!(
                            CAT,
                            obj = element,
                            "Failed to set '{}' sockopt value '{}': {}",
                            key,
                            value,
                            err
                        );
                    }
                }
                "pbkeylen" => {
                    let v = match value.parse::<i32>() {
                        Ok(v) => v,
                        Err(err) => {
                            gst::warning!(
                                CAT,
                                obj = element,
                                "Failed to parse '{}' value '{}': {}",
                                key,
                                value,
                                err
                            );
                            continue;
                        }
                    };

                    if let Err(err) = socket.set_sockopt::<srt::PbKeylen>(v) {
                        gst::warning!(
                            CAT,
                            obj = element,
                            "Failed to set '{}' sockopt value '{}': {}",
                            key,
                            value,
                            err
                        );
                    }
                }
                _ => {
                    gst::warning!(CAT, obj = element, "Unsupported parameter {}", key);
                }
            }

            // TODO: support more parameters
        }

        socket.set_sockopt::<srt::SndSyn>(true).map_err(|err| {
            gst::error_msg!(
                gst::ResourceError::OpenRead,
                ["Failed to mark socket non-blocking: {}", err]
            )
        })?;
        socket.set_sockopt::<srt::RcvSyn>(true).map_err(|err| {
            gst::error_msg!(
                gst::ResourceError::OpenRead,
                ["Failed to mark socket non-blocking: {}", err]
            )
        })?;

        poll.set_usock(
            socket,
            srt::Flags::IN
                | srt::Flags::OUT
                | srt::Flags::ERR
                | srt::Flags::UPDATE
                | srt::Flags::ET,
        )
        .map_err(|err| {
            gst::error_msg!(
                gst::ResourceError::OpenRead,
                ["Failed to add socket to epoll: {}", err]
            )
        })?;

        match mode {
            Mode::Listener => {
                poll.set_sockopt::<srt::ReuseAddr>(true).map_err(|err| {
                    gst::error_msg!(
                        gst::ResourceError::OpenRead,
                        ["Failed to set ReuseAddr: {}", err]
                    )
                })?;

                poll.bind(parsed_uri.socket_addr).map_err(|err| {
                    gst::error_msg!(
                        gst::ResourceError::OpenRead,
                        ["Failed to bind to {}: {}", parsed_uri.socket_addr, err]
                    )
                })?;

                poll.listen(1).map_err(|err| {
                    gst::error_msg!(gst::ResourceError::OpenRead, ["Failed to listen: {}", err])
                })?;

                gst::debug!(CAT, obj = element, "Waiting for client connection");
                loop {
                    let event = poll.uwait(100).map_err(|err| {
                        gst::error_msg!(
                            gst::ResourceError::OpenRead,
                            ["Failed to wait for client connection: {}", err]
                        )
                    })?;

                    if receiver.0.shutdown.load(atomic::Ordering::SeqCst) {
                        gst::debug!(CAT, obj = element, "Shutting down");
                        return Ok(None);
                    }

                    let event = match event {
                        None => continue,
                        Some(event) => event,
                    };

                    if event.events().contains(srt::Flags::IN) {
                        break;
                    }
                    if event.events().contains(srt::Flags::ERR) {
                        return Err(gst::error_msg!(
                            gst::ResourceError::OpenRead,
                            ["Failed to wait for client connection"]
                        ));
                    }
                }

                let (mut client_socket, client_addr, client_port) =
                    poll.accept().map_err(|err| {
                        gst::error_msg!(
                            gst::ResourceError::OpenRead,
                            ["Failed to accept client connection: {}", err]
                        )
                    })?;

                gst::debug!(
                    CAT,
                    obj = element,
                    "Client {}:{} connected",
                    client_addr,
                    client_port
                );

                client_socket
                    .set_sockopt::<srt::SndSyn>(true)
                    .map_err(|err| {
                        gst::error_msg!(
                            gst::ResourceError::OpenRead,
                            ["Failed to mark socket non-blocking: {}", err]
                        )
                    })?;
                client_socket
                    .set_sockopt::<srt::RcvSyn>(true)
                    .map_err(|err| {
                        gst::error_msg!(
                            gst::ResourceError::OpenRead,
                            ["Failed to mark socket non-blocking: {}", err]
                        )
                    })?;

                poll.set_usock(
                    client_socket,
                    srt::Flags::IN
                        | srt::Flags::OUT
                        | srt::Flags::ERR
                        | srt::Flags::UPDATE
                        | srt::Flags::ET,
                )
                .map_err(|err| {
                    gst::error_msg!(
                        gst::ResourceError::OpenRead,
                        ["Failed to add socket to epoll: {}", err]
                    )
                })?;
            }
            Mode::Caller | Mode::Rendezvous => {
                if matches!(mode, Mode::Rendezvous) {
                    poll.set_sockopt::<srt::Rendezvous>(true).map_err(|err| {
                        gst::error_msg!(
                            gst::ResourceError::OpenRead,
                            ["Failed to mark socket non-blocking: {}", err]
                        )
                    })?;

                    poll.set_sockopt::<srt::ReuseAddr>(true).map_err(|err| {
                        gst::error_msg!(
                            gst::ResourceError::OpenRead,
                            ["Failed to set ReuseAddr: {}", err]
                        )
                    })?;

                    poll.bind(parsed_uri.socket_addr).map_err(|err| {
                        gst::error_msg!(
                            gst::ResourceError::OpenRead,
                            ["Failed to bind to {}: {}", parsed_uri.socket_addr, err]
                        )
                    })?;
                }

                let res = poll.connect(parsed_uri.socket_addr);

                if let Err(err) = res {
                    if matches!(err, srt::Error::CONNREJ) {
                        if let Err(reason) = poll.reject_reason() {
                            gst::warning!(CAT, obj = &element, "Connection failed: {}", reason);

                            // Allow retrying on Timeout / Congestion
                            if matches!(
                                reason,
                                srt::RejectReason::Timeout | srt::RejectReason::Congestion
                            ) {
                                receiver.enqueue(QueueItem::Error(srt::Error::CONNSETUP));
                                return Ok(None);
                            } else {
                                return Err(gst::error_msg!(
                                    gst::ResourceError::OpenRead,
                                    [
                                        "Failed to connect to {}: {} ({})",
                                        parsed_uri.socket_addr,
                                        err,
                                        reason
                                    ]
                                ));
                            }
                        } else {
                            gst::warning!(CAT, obj = &element, "Connection failed");

                            return Err(gst::error_msg!(
                                gst::ResourceError::OpenRead,
                                ["Failed to connect to {}: {}", parsed_uri.socket_addr, err,]
                            ));
                        }
                    } else if matches!(err, srt::Error::TIMEOUT | srt::Error::NOSERVER) {
                        // Allow retrying on Timeout / No server response
                        receiver.enqueue(QueueItem::Error(srt::Error::CONNSETUP));
                        return Ok(None);
                    } else {
                        return Err(gst::error_msg!(
                            gst::ResourceError::OpenRead,
                            ["Failed to connect to {}: {}", parsed_uri.socket_addr, err]
                        ));
                    }
                }

                gst::debug!(
                    CAT,
                    obj = element,
                    "Waiting for connection to {} to be established",
                    parsed_uri.socket_addr,
                );
                loop {
                    let event = poll.uwait(100).map_err(|err| {
                        gst::error_msg!(
                            gst::ResourceError::OpenRead,
                            ["Failed to wait for client connection: {}", err]
                        )
                    })?;

                    if receiver.0.shutdown.load(atomic::Ordering::SeqCst) {
                        gst::debug!(CAT, obj = element, "Shutting down");
                        return Ok(None);
                    }

                    let event = match event {
                        None => continue,
                        Some(event) => event,
                    };

                    if event.events().contains(srt::Flags::OUT) {
                        break;
                    }
                    if event.events().contains(srt::Flags::ERR) {
                        if let Err(reason) = poll.reject_reason() {
                            // Allow retrying on Timeout / Congestion
                            if matches!(
                                reason,
                                srt::RejectReason::Timeout | srt::RejectReason::Congestion
                            ) {
                                receiver.enqueue(QueueItem::Error(srt::Error::CONNSETUP));
                                return Ok(None);
                            } else {
                                return Err(gst::error_msg!(
                                    gst::ResourceError::OpenRead,
                                    [
                                        "Failed to wait for connection to be established: {}",
                                        reason
                                    ]
                                ));
                            }
                        } else {
                            return Err(gst::error_msg!(
                                gst::ResourceError::OpenRead,
                                ["Failed to wait for connection to be established",]
                            ));
                        }
                    }
                }
            }
        }

        Ok(Some(poll))
    }

    fn read_packet(
        element: &super::SrtSrc,
        receiver: &Receiver,
        poll: &mut Poll,
    ) -> Result<bool, gst::ErrorMessage> {
        loop {
            let event = poll.uwait(100).map_err(|err| {
                gst::error_msg!(
                    gst::ResourceError::OpenRead,
                    ["Failed to wait for packet: {}", err]
                )
            })?;

            if receiver.0.shutdown.load(atomic::Ordering::SeqCst) {
                gst::debug!(CAT, obj = element, "Shutting down");
                return Ok(true);
            }

            let event = match event {
                None => continue,
                Some(event) => event,
            };

            if event.events().contains(srt::Flags::IN) {
                break;
            }
            if event.events().contains(srt::Flags::ERR) {
                return Err(gst::error_msg!(
                    gst::ResourceError::OpenRead,
                    ["Failed to wait for packet"]
                ));
            }
        }

        loop {
            let mut buffer = receiver
                .0
                .buffer_pool
                .acquire_buffer(None)
                .map_err(|_err| {
                    gst::error_msg!(gst::ResourceError::OpenRead, ["Failed to acquire buffer"])
                })?;
            let buffer_ref = buffer.get_mut().unwrap();
            let mut data = buffer_ref.map_writable().unwrap();

            let (len, info) = match poll.recvmsg(&mut data) {
                Ok(res) => res,
                Err(srt::Error::ASYNCRCV) => return Ok(false),
                Err(err) => {
                    receiver.enqueue(QueueItem::Error(err));
                    return Ok(true);
                }
            };

            if len == 0 {
                receiver.enqueue(QueueItem::Eos);
                return Ok(true);
            }

            drop(data);
            buffer_ref.set_size(len);

            receiver.enqueue(QueueItem::Data {
                buffer,
                seqno: info.pkgseq(),
                msgno: info.msgno(),
                srctime: info.srctime(),
            });
        }
    }

    fn run(receiver: Receiver) {
        let element = match receiver.0.element.upgrade() {
            Some(element) => element,
            None => return,
        };

        gst::debug!(
            CAT,
            obj = &element,
            "Starting receiver for URI {:?}",
            receiver.0.parsed_uri
        );

        let mut poll = match Self::setup(&element, &receiver) {
            Ok(None) => return,
            Ok(Some(res)) => res,
            Err(err) => {
                gst::error!(CAT, obj = &element, "{err}");
                element.post_error_message(err);
                return;
            }
        };

        while !receiver.0.shutdown.load(atomic::Ordering::SeqCst) {
            match Self::read_packet(&element, &receiver, &mut poll) {
                Ok(true) => break,
                Ok(false) => continue,
                Err(err) => {
                    gst::error!(CAT, obj = &element, "{err}");
                    element.post_error_message(err);
                    break;
                }
            }
        }

        gst::debug!(CAT, obj = &element, "Stopped receiver");
    }
}

enum Mode {
    Caller,
    Listener,
    Rendezvous,
}

impl std::str::FromStr for Mode {
    type Err = gst::ErrorMessage;

    fn from_str(mode: &str) -> Result<Self, Self::Err> {
        match mode {
            "caller" => Ok(Mode::Caller),
            "listener" => Ok(Mode::Listener),
            "rendezvous" => Ok(Mode::Rendezvous),
            _ => Err(gst::error_msg!(
                gst::ResourceError::OpenRead,
                ["{} is not valid SRT mode", mode]
            )),
        }
    }
}

#[derive(Debug, Clone)]
struct ParsedUri {
    host: Option<String>,
    socket_addr: std::net::SocketAddr,
    latency: u32,
    parameters: Vec<(String, String)>,
}

impl std::str::FromStr for ParsedUri {
    type Err = gst::ErrorMessage;

    fn from_str(uri: &str) -> Result<Self, Self::Err> {
        let s = match uri.strip_prefix("srt://") {
            None => {
                return Err(gst::error_msg!(
                    gst::ResourceError::OpenRead,
                    ["{} not an SRT URI", uri]
                ));
            }
            Some(s) => s,
        };

        let (host_port, parameters) = match s.split_once('?') {
            Some(res) => res,
            None => (s, ""),
        };

        let host_port = host_port
            .split_once('/')
            .map(|(fst, _)| fst)
            .unwrap_or(host_port);

        let (host, port) = match host_port.split_once(':') {
            Some(res) => res,
            None => {
                return Err(gst::error_msg!(
                    gst::ResourceError::OpenRead,
                    ["{} does not contain a port", uri]
                ));
            }
        };

        let host = host.trim();
        let port = port.trim();
        let port = port.parse::<u16>().map_err(|err| {
            gst::error_msg!(
                gst::ResourceError::OpenRead,
                ["{} does not contain a valid port number: {}", uri, err]
            )
        })?;

        let (socket_addr, host) = if host.is_empty() {
            (
                std::net::SocketAddr::V4(std::net::SocketAddrV4::new(
                    std::net::Ipv4Addr::UNSPECIFIED,
                    port,
                )),
                None,
            )
        } else {
            use std::net::ToSocketAddrs;

            (
                (host, port)
                    .to_socket_addrs()
                    .and_then(|mut it| {
                        it.next()
                            .ok_or_else(|| std::io::Error::from(std::io::ErrorKind::NotFound))
                    })
                    .map_err(|err| {
                        gst::error_msg!(
                        gst::ResourceError::OpenRead,
                        [
                            "{} does not contain a valid IP address or resolveable hostname: {}",
                            uri,
                            err
                        ]
                    )
                    })?,
                Some(host.to_owned()),
            )
        };

        let mut split_parameters = Vec::new();
        for parameter in parameters.trim().split('&') {
            let parameter = parameter.trim();
            if parameter.is_empty() {
                continue;
            }

            let (key, value) = match parameter.trim().split_once('=') {
                Some(res) => res,
                None => {
                    return Err(gst::error_msg!(
                        gst::ResourceError::OpenRead,
                        ["{} contains an invalid parameter '{}'", uri, parameter]
                    ))
                }
            };

            let key = key.trim();
            let value = value.trim();

            let key = percent_encoding::percent_decode_str(key);
            let value = percent_encoding::percent_decode_str(value);

            split_parameters.push((
                String::from(key.decode_utf8().map_err(|err| {
                    gst::error_msg!(
                        gst::ResourceError::OpenRead,
                        [
                            "{} contains an invalid parameter '{}' key: {}",
                            uri,
                            parameter,
                            err
                        ]
                    )
                })?),
                String::from(value.decode_utf8().map_err(|err| {
                    gst::error_msg!(
                        gst::ResourceError::OpenRead,
                        [
                            "{} contains an invalid parameter '{}' value: {}",
                            uri,
                            parameter,
                            err
                        ]
                    )
                })?),
            ));
        }

        let latency = split_parameters
            .iter()
            .find(|(key, _value)| key == "latency")
            .map(|(_key, latency)| {
                latency.parse::<u32>().map_err(|err| {
                    gst::error_msg!(
                        gst::ResourceError::OpenRead,
                        ["{} contains an invalid latency value: {}", uri, err]
                    )
                })
            })
            .transpose()?
            .unwrap_or(120);

        Ok(ParsedUri {
            host,
            socket_addr,
            latency,
            parameters: split_parameters,
        })
    }
}

#[derive(Debug)]
struct Poll {
    epoll: srt::EPoll,
    socket: Option<srt::Socket>,
}

#[allow(dead_code)]
impl Poll {
    fn new() -> Result<Self, srt::Error> {
        srt::EPoll::new().map(|epoll| Self {
            epoll,
            socket: None,
        })
    }

    fn set_usock(&mut self, socket: srt::Socket, flags: srt::Flags) -> Result<(), srt::Error> {
        unsafe {
            if let Some(socket) = self.socket.take() {
                let _ = self.epoll.remove_usock(&socket);
            }
            self.epoll.add_usock(&socket, flags)?;
            self.socket = Some(socket);
            Ok(())
        }
    }

    fn remove_usock(&mut self) -> Result<(), srt::Error> {
        unsafe {
            if let Some(socket) = self.socket.take() {
                self.epoll.remove_usock(&socket)?;
            }
            Ok(())
        }
    }

    fn uwait(&mut self, ms_timeout: i64) -> Result<Option<&srt::EPollEvent>, srt::Error> {
        let res = self.epoll.uwait(ms_timeout)?;
        Ok(res.first())
    }

    pub fn set_sockopt<'a, S: srt::SockOpt<'a>>(
        &mut self,
        value: S::Type,
    ) -> Result<(), srt::Error> {
        if let Some(ref mut socket) = self.socket {
            socket.set_sockopt::<S>(value)
        } else {
            Err(srt::Error::INVSOCK)
        }
    }

    pub fn get_sockopt<'a, S: srt::SockOpt<'a>>(&self) -> Result<S::Type, srt::Error> {
        if let Some(ref socket) = self.socket {
            socket.get_sockopt::<S>()
        } else {
            Err(srt::Error::INVSOCK)
        }
    }

    pub fn bind(&mut self, addr: std::net::SocketAddr) -> Result<(), srt::Error> {
        if let Some(ref mut socket) = self.socket {
            socket.bind(addr)
        } else {
            Err(srt::Error::INVSOCK)
        }
    }

    pub fn listen(&mut self, backlog: i32) -> Result<(), srt::Error> {
        if let Some(ref mut socket) = self.socket {
            socket.listen(backlog)
        } else {
            Err(srt::Error::INVSOCK)
        }
    }

    pub fn accept(&mut self) -> Result<(srt::Socket, std::net::IpAddr, u16), srt::Error> {
        if let Some(ref mut socket) = self.socket {
            socket.accept()
        } else {
            Err(srt::Error::INVSOCK)
        }
    }

    pub fn connect(&mut self, addr: std::net::SocketAddr) -> Result<(), srt::Error> {
        if let Some(ref mut socket) = self.socket {
            socket.connect(addr)
        } else {
            Err(srt::Error::INVSOCK)
        }
    }

    pub fn getpeername(&self) -> Result<(std::net::IpAddr, u16), srt::Error> {
        if let Some(ref socket) = self.socket {
            socket.getpeername()
        } else {
            Err(srt::Error::INVSOCK)
        }
    }

    pub fn getsockname(&self) -> Result<(std::net::IpAddr, u16), srt::Error> {
        if let Some(ref socket) = self.socket {
            socket.getsockname()
        } else {
            Err(srt::Error::INVSOCK)
        }
    }

    pub fn sendmsg(&mut self, buf: &[u8], ttl: i32, inorder: bool) -> Result<usize, srt::Error> {
        if let Some(ref mut socket) = self.socket {
            socket.sendmsg(buf, ttl, inorder)
        } else {
            Err(srt::Error::INVSOCK)
        }
    }

    pub fn recvmsg(&mut self, buf: &mut [u8]) -> Result<(usize, srt::RecvInfo), srt::Error> {
        if let Some(ref mut socket) = self.socket {
            socket.recvmsg(buf)
        } else {
            Err(srt::Error::INVSOCK)
        }
    }

    pub fn reject_reason(&self) -> Result<(), srt::RejectReason> {
        if let Some(ref socket) = self.socket {
            socket.reject_reason()
        } else {
            Err(srt::RejectReason::Unknown)
        }
    }

    pub fn get_sockstate(&self) -> Option<srt::SockStatus> {
        self.socket.as_ref().map(|s| s.get_sockstate())
    }
}

impl Drop for Poll {
    fn drop(&mut self) {
        unsafe {
            if let Some(socket) = self.socket.take() {
                let _ = self.epoll.remove_usock(&socket);
            }
        }
    }
}
